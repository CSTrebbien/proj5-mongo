"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask 
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging
import os
from pymongo import MongoClient

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404




###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', "", type=float)
    distance = request.args.get("distance", "", type=float)
    date = request.args.get("sdate","", type = str)
    time = request.args.get("stime","", type = str)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    mtime = arrow.get(date + " " + time,'YYYY-MM-DD HH:mm').isoformat()

    open_time = acp_times.open_time(km, distance, mtime)
    close_time = acp_times.close_time(km, distance, mtime)
    result = {"open": open_time, "close": close_time, "time": time , "date": date, "mtime":mtime}
    return flask.jsonify(result=result)

@app.route("/_mysubmit")
def _mysubmit():
    db.tododb.drop()

    km = request.args.get('km', "", type=float)
    opentime= request.args.get('opentime', "", type=str)
    closetime = request.args.get('opentime', "", type=str)

    result = 0
    item_doc = {
        'km': km,
        'opentime': opentime,
        'closetime': closetime
    }

    db.tododb.insert_one(item_doc)

    if (db.togodb.find()):
        #db.drop_database("togodb")
        result = 1

    return flask.jsonify(result = result)


@app.route("/display")
def display(): 
    _items = db.tododb.find()
    items = [item for item in _items]
    #flask.render_template("todo.html")
    #render_template('todo.html', items=items)

    return  render_template('display.html', items=items)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
